#Name: Sam LeCompte
#Date: 10/15/15
#Project: Trivia Project

import question

def setPlayerPoints():
    pList = list()
    numPlayers = int(input("How many players are there?  "))
    for x in range(0,numPlayers):
        pList.append(0)
    return pList
        

playerList = setPlayerPoints()


questionList = list()
questionList.append(question.Question("On Good Friday in 1930, the BBC reported, There is no news. Instead they played? ","1.Heavy Metal Music", "2.piano music","3.Nothing","4.yo momma",2))
questionList.append(question.Question(" M&M's actually stands for?", "1.yo momma", "2.Mac and monkey", "3.mmmm and mmmm", "4.Mars and Murrie's",4))
questionList.append(question.Question("What was Kool-Aid orginally named?","1.Juicy Jucie","2.Lean in my cup", "3.Fruit Smack", "4.yo momma",3))
questionList.append(question.Question("How much money does Judge Judy make a year?","1.yo momma","2.$45 million","3.$20 million","4.$1.2 million",2))
questionList.append(question.Question("Who gave birth to you?","1.Steve", "2.Jane","3.Phillip","4.yo momma",4))

turn = 0

for q in questionList:
    q.printQuestion()
    answer = int(input("Take a guess between 1 and 4 (i.e. Player " + str(turn+1) + "):  "))
    if(q.isCorrect()==answer):
        print("There you go G!")
        playerList[turn] += 1
    else:
        print("Sorry B.")
        
    if(turn < len(playerList)-1):
        turn += 1
    else:
        turn = 0


print(playerList)

highestPoints = 0
winner = 0
index =0
for points in playerList:
    if(points > highestPoints):
        highestPoints = points
        winner = index
    index += 1

print("Player", winner+1, "is the winner with", highestPoints, "points")

        
        
    

    
